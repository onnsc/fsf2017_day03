//load libraries
var path = require("path");
var express = require("express");

//create an instance of the app
var app = express();


var message=["one", "two", "three", "four", "five"]; //the same applies to images
var msgIdx = 0;

//load the static resources from the following directory
//static means it will only pick up files under/below the stated folder
//__dirname refers to the root file that you are running at
//can replace __dirname with other folder directory or google drive, as appropriate
// app.use(express.static(__dirname+"/public"));  //string concate isnt the best, use path join below
//path join dont require you to know how the path is constructed, the function will construct it for you
app.use(express.static(path.join(__dirname,"public")));

app.use("/picture",function(req,res){
//   var idx= Math.round(Math.random()*message.length)% message.length;
    res.status(202); //html status code
    res.type("text/html"); //MINE type 
    res.send("<h1>"+ message[msgIdx]+"</h1>");
msgIdx = (msgIdx + 1) % message.length;//modulo% maths is applied extensively in placement
/* to get the numbers in sequence instead of random, replace message[msgIdx] on line 26 with message[Idx]
 unblock line 23*/
     
});

//how to access upper level picture folder:
app.use(express.static(path.join(__dirname,"picture"))); //where public and picture folders are on the same level

//replacing res.send text message with random images
var asset=["asset0.jpg","asset1.jpg","asset2.jpg","asset3.jpg","asset4.jpg"];

app.use(function(req,res){
    var i= Math.round(Math.random()*asset.length)%asset.length;
    res.status(404);
    res.type("text/html");
    res.send("<img src=/" + asset[i]+">");
});



/*app.use(function(req,res){
    res.status(404);
    res.send("error file not found");
});*/


//Define the port
app.set("port", parseInt(process.argv[2]) || parseInt(process.env.APP_PORT) || 3000);

//start the server
app.listen(app.get("port"), function(){
    console.log("Application started at %s on %d", new Date(), app.get("port"));
})