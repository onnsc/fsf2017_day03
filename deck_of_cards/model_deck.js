var express = require("express");
var path = require("path");

var app = express();

app.use(express.static(path.join(__dirname,"public")));

//run this script on ARC

//create deck
// var variablename = function (parameters / arguments / values)
var deckOfCards = function(shuf) {
    var suit = ["Club", "Spade", "Diamond", "Hearts"];
    var name = ["Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King"];
    var value = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10];

    var theDeck = [];

    //Create the deck
    for (var s in suit) {
        for (var n in name)
            theDeck.push({
                suit: suit[s],
                name: name[n],
                value: value[n]
            });
    }
    
    //Shuffle
    shuf = shuf || 3;
    for (var times = 0; times < shuf; times++)
        for (var i = 0; i < theDeck.length; i++) {
            var dst = parseInt(Math.random() * theDeck.length)
            var swapVal = theDeck[i];
            theDeck[i] = theDeck[dst];
            theDeck[dst] = swapVal;
        }

    return (theDeck);
}

var taitee = deckOfCards();
var x = taitee.length;
console.log(taitee);


app.get("/cards", function(req,resp){
    var taitee = deckOfCards();
    resp.status(200)
        .type("application/json")
        .json(taitee);
    });

app.get("/card/:noOfCard",function(req,resp){
    var taitee = deckOfCards();
    var noOfCard = req.params.noOfCard;
    var cardNo = [];
    var arr=[]; //create a new array for the selected cards
    
    for (cardNo=0; cardNo<noOfCard; cardNo++){
        arr.push(taitee[cardNo]);
    }
        resp.status(200)
            .type("application/json")
            .json(arr);
    });

app.use(function(req,resp){   //why doesn't app.GET work here?
    resp.status(404)
        .type("text/html")
        .send("<h3>Sorry...Better Luck Next Time</h3>");
    });



/*app.get("/cards/:empNo/:infoType", function(req,resp){
    var empNo = req.params.empNo;
    var infoType = req.params.infoType;

*/


app.set("port", process.env.APP_PORT || 3000);

app.listen(app.get("port"),function(){
    console.log("Application is connected to the port at %s on port %d", new Date(), app.get("port"))});