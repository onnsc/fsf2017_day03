var person = {      //notice the {} used in object as opposed to [] in array
   name: "fred",
   email: "fred@gmail.com", 
   family: [        //an array within an object
    {name: "wilma", email:"wilma@bedrock.com"},   //var f = new Array(); behaves like array but it not array
    {name: "jon", email:"jon@bedrock.com"}, 
   ],
   "-password": "sdfsdf"
};

console.log(["-password"]) //use []

//to print as a list
person.city = "bedrock" //is the same as writing like the next line
person["postal"] = 12345;
for (var i in person)
    console.log("%s = %s", i, person[i]);

console.log("-----------------FUNCTIONS-------------------")

//function defines a behavior. Function as be placed into an ARRAY like a VALUE
function hello(name, name2){     //this is the shortcut of var hello = function(name)
    name = name || "Joe";
    console.log("hello there %s", name);
    }

//var fred = {name: "fred"} conceptually they are they same
hello("fred", "barney") //invoking aka executing the function above
hello()

var fred = {
    name: "fred",
    greet: hello
}
console.log("%s >>> %s", fred.name, fred.greet) //to print an object
//essentially console is an object like fred and .log is a function
//See functions as just data and values, good for angular


var people = ["fred", "barney", "wilma", "betty"]

for (var i =0; i < people.length; i++)
    console.log(people[i]);

////another way
//don't need to know the length
//don't need to initialize i
//just need to specify what to do
//need to add v?
people.forEach(function(v,i){
console.log("%s",v)
})

////try to see the similarity with below
var num = [2,3,4,5]

num.forEach(function(v,i){ //forEach is essentially APPLY
    console.log(v*2)
});

//Function Chaining: Tagging several functions (.map .filter .forEach)
//Function chaining works if the return output is the same object
num2= num
    .map(function(v){  //creating a new array through .map (from one domain to another domains)
    return(v*2)
    })
    .filter(function(v){
    return(v>=5)    
    })
    .forEach(function(v){   //to print out the whole array, line by line
        console.log("foreach: %d", v)
    })

console.log(num2)

// same way to write it
//filter(map(function(v)))
//apply(filter,apply(map,_))

var rand = function(give_num){
    give_num(Math.random());
}

rand(function(r_v){
    var idx=Math.round(r_v*people.length);
    console.log("random name: %s", people[idx])
})

//------------------this.---------------------

var fred = {                                                                                        
    name: "fred",                                                                                    
    email: "fred@gmail.com",                                                                        
    greet: function() {                                                                              
        console.log("Hello my test name is %s", this.name);                                              
    }                                                                                                
};                                                                                                  
                                                                                                   
name = "BARNEY";                                                                                    
                                                                                                   
var greet = fred.greet;                                                                              
                                                                                                   
fred.greet()                                                                                        
                                                                                                   
console.log("Executing greet")                                                                      
                                                                                                   
greet();                                                                                            
                                                                                                   
fred.greet()

//THIS always belong to the current OWNER of the function
